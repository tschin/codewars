std::string decodeMorse(std::string morseCode)
{
    std::string decoded;
    auto it = morseCode.begin();

    while (it < morseCode.end())
    {

        std::string encodeSubStr;
        while (*it != ' ' && it != morseCode.end())
        {
            encodeSubStr += *it;
            it++;
        }

        decoded += MORSE_CODE[encodeSubStr];

        if (decoded.empty())
        {
            it++;
            continue;
        }

        int countSpacesForSpace = 3;
        int currentCountsSpaces = 0;

        while (*it == ' ' && it != morseCode.end())
        {
            currentCountsSpaces++;
            it++;
        }

        if (currentCountsSpaces == countSpacesForSpace)
        {
            decoded += " ";
        }
    }

    return decoded;
}