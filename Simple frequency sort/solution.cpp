#include <set>

std::vector<int> solve(const std::vector<int> &vec) {
    std::vector<int> result;

    std::map<int, int> frequency;
    for(auto value: vec) {
        if (frequency.count(value) == 0) {
            frequency[value] = 1;
        } else {
            frequency[value]++;
        }
    }

    std::vector<std::pair<int,int>> sortedByFrequency;
    for (auto pair: frequency) {
        sortedByFrequency.push_back(std::make_pair(pair.first, pair.second));
    }
    auto comparator = [](std::pair<int,int> const & a, std::pair<int,int> const & b)
    {
        return a.second > b.second;
    };
    std::sort(sortedByFrequency.begin(), sortedByFrequency.end(), comparator);

    int countInclusions = sortedByFrequency.begin()->second;

    do {
        std::vector<int> needSort;
        std::set<int> sorted;

        for(auto pair: sortedByFrequency) {
            if (pair.second == countInclusions) {
                sorted.insert(pair.first);
            }
        }

        for (int sortedValue: sorted) {
            for(int i=0; i<countInclusions; i++) {
                result.push_back(sortedValue);
            }
        }

        countInclusions--;
    } while (countInclusions>=1);

    return result;
}