#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>
#include <numeric>

using namespace std;

struct Time {
    int h = 0;
    int m = 0;
    int s = 0;

    int sumSeconds = 0;
};


int getSeconds(Time time)
{
    int seconds = 0;

    seconds += time.s;
    seconds += time.m * 60;
    seconds += time.h * 60 * 60;

    return seconds;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::stringstream ss(s);
    std::string item;
    std::vector<std::string> elems;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::string replace(std::string str, std::string from, std::string to)
{
    auto n = str.find(from);

    while(n != std::string::npos) {
        str.replace(n, from.size(), to);
        n = str.find(from);
    }
    return str;
}

std::vector<Time> getTimes(std::string str)
{
    std::vector<Time> times;

    std::string l = replace(str, " ", "");

    auto splited = split(l, ',');
    for(auto str: splited) {

        auto numbers = split(str, '|');
        Time time;
        time.h = std::stoi(numbers[0]);
        time.m = std::stoi(numbers[1]);
        time.s = std::stoi(numbers[2]);

        time.sumSeconds = getSeconds(time);
        times.push_back(time);
    }


    return times;
}

std::vector<int> getSecondsList(std::vector<Time> times)
{
    std::vector<int> secondsList;
    for (auto time: times) {
        secondsList.push_back(time.sumSeconds);
    }
    return secondsList;
}
std::string toFormattedString(int value)
{
    std::string formattedString;

    if(value<10) {
        formattedString+="0";
    }

    return formattedString+std::to_string(value);
}

std::string getFormattedTimeString(int seconds)
{
    Time time;
    time.h = seconds / 3600;
    time.m = (seconds - time.h * 3600)/60;
    time.s = seconds - (time.h * 3600 + time.m*60);

    return toFormattedString(time.h)+"|"+toFormattedString(time.m)+"|"+toFormattedString(time.s);
}

std::string getRangeString(int seconds)
{
    return "Range: "+getFormattedTimeString(seconds);
}

std::string getAverageString(int seconds)
{
    return "Average: "+getFormattedTimeString(seconds);
}

std::string getMedianString(int seconds)
{
    return "Median: "+getFormattedTimeString(seconds);
}

int range(std::vector<int> &sortedSeconds)
{
    return sortedSeconds.back() - sortedSeconds.front();
}

int average(std::vector<int> &sortedSeconds)
{
    return std::accumulate(sortedSeconds.begin(), sortedSeconds.end(), 0)/sortedSeconds.size();
}

int median(std::vector<int> &sortedSeconds)
{
    if (sortedSeconds.size() % 2 == 0) {
        const auto median_it1 = sortedSeconds.begin() + sortedSeconds.size() / 2 - 1;
        const auto median_it2 = sortedSeconds.begin() + sortedSeconds.size() / 2;
        return (*median_it1 + *median_it2) / 2;

    } else {
        const auto median_it = sortedSeconds.begin() + sortedSeconds.size() / 2;
        return *median_it;
    }
}

class Stat
{
public:
  static std::string stat(const std::string &strg) {
    
    std::string stat;
    auto secondsList = getSecondsList(getTimes(strg));

    std::sort(secondsList.begin(), secondsList.end());

    stat +=  getRangeString(range(secondsList));
    stat += " ";
    stat += getAverageString(average(secondsList));
    stat += " ";
    stat += getMedianString(median(secondsList));

    return stat;
  };
};