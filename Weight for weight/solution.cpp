#include <iostream>
#include <vector>
#include <sstream>
#include <map>
#include <algorithm>

using namespace std;

vector<string> split(string str) {
    vector<string> numbers;
    char delimiter = ' ';
    std::vector<char> data;
    std::string token;
    std::istringstream tokenStream(str);
    while (std::getline(tokenStream, token, delimiter))
    {
        numbers.push_back(token);
    }

    return numbers;
}

int getSum(string str) {
    int sum = 0;
    for(auto c: str) {
        sum+=c - '0';
    }
    return sum;
}

string getOrderedString(vector<pair<int, string>> &sorted)
{
    string orderedStr;

    for (auto it = sorted.begin(); it != sorted.end(); ++it ) {
        
        orderedStr += it->second;
        
        if (it != --sorted.end()) {
            orderedStr += " ";
        }
    }   
    
    return orderedStr;
}

class WeightSort
{
public:
    
    static std::string orderWeight(const std::string &strng)
    {
        vector<pair<int, string>> unsorted;
        
        auto numbers = split(strng);

        for (string number: numbers) {
            int sum = getSum(number);
            unsorted.push_back(make_pair(sum, number));
        }

        sort(unsorted.begin(), unsorted.end(), [&](pair<int, string> a, pair<int, string> b) { 
            return a.first < b.first || (a.first == b.first && a.second < b.second);
        });

        return getOrderedString(unsorted);
    };
};