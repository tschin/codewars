#include <iostream>
#include "stdio.h"
#include <map>
#include <vector>
#include <string>
#include <iterator>

using namespace std;


std::map<int, std::string> getMapTemplateByCountNames()
{
    return {
        {0,"no one likes this"},
        {1,"% likes this"},
        {2,"% and % like this"},
        {3,"%, % and % like this"},
        {4,"%, % and & others like this"},
    };
}

std::string getTemplate(const std::vector<std::string> &names)
{
    int countNames = names.size();
    std::string likesTemplate;

    auto mapTemplateByCountNames = getMapTemplateByCountNames();

    if (mapTemplateByCountNames.find(countNames) != mapTemplateByCountNames.end()) {
        likesTemplate = mapTemplateByCountNames[countNames];
    } else if (countNames > 4) {
        likesTemplate = mapTemplateByCountNames[4];
    }

    return likesTemplate;
}

std::string fillTemplate(std::string likesTemplate, const std::vector<std::string> &names)
{
    if (names.size() == 0) {
        return likesTemplate;
    }
    std::string filledTemplate = likesTemplate;

    std::vector<std::string>::const_iterator endIterator = names.size() >= 4 ? names.begin()+2 : names.end();

    std::vector<std::string> printedNames(names.begin(), endIterator);

    for (auto name: printedNames) {
         auto n = filledTemplate.find("%");
         filledTemplate.replace(n, 1, name);
    }

    auto n = filledTemplate.find("&");

    if (n != std::string::npos) {
        std::string size = std::to_string(names.size()-2);
        filledTemplate.replace(n, 1, size);
    }

    return filledTemplate;
}

std::string likes(const std::vector<std::string> &names)
{
    return fillTemplate(getTemplate(names),  names);
}